/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * Copyright (c) 2021 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "utils.hpp"

#include <iostream>
#include <vector>
#include <sstream>

#include <boost/program_options.hpp>

using std::cout;
using std::cerr;
using std::endl;

namespace po = boost::program_options;

uint8_t gen_random_value_255()
{
    // Marsaglia's xorshf generator
    static unsigned long x = 123456789, y = 362436069, z = 521288629;

    unsigned long t;

    x ^= x << 16;
    x ^= x >> 5;
    x ^= x << 1;

    t = x;
    x = y;
    y = z;
    z = t ^ x ^ y;

    return (z & 0x0FF);
}

std::vector<uint8_t> create_random_bytes(std::size_t n)
{
    std::vector<uint8_t> res(n);

    for (uint8_t& v : res)
        v = gen_random_value_255();

    return res;
}

void run_memcpy(std::size_t n, output_t ot)
{
    auto bytes = create_random_bytes(n);
    std::vector<uint8_t> copied(bytes.size(), 0);

    std::ostringstream os;
    os << "std::memcpy," << n;

    {
        stack_printer __stack_printer__(os.str(), ot);
        std::memcpy(copied.data(), bytes.data(), bytes.size());
    }

    if (bytes != copied)
        throw std::runtime_error("copy failed.");
}

void run_copy(std::size_t n, output_t ot)
{

    auto bytes = create_random_bytes(n);
    std::vector<uint8_t> copied(bytes.size(), 0);

    std::ostringstream os;
    os << "std::copy," << n;

    {
        stack_printer __stack_printer__(os.str(), ot);
        std::copy(bytes.data(), bytes.data() + bytes.size(), copied.data());
    }

    if (bytes != copied)
        throw std::runtime_error("copy failed.");
}

int main(int argc, char** argv)
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Print this help.")
        ("output-type,t", po::value<std::string>()->default_value("section"), "Runtime output type.")
        ("size,n", po::value<std::size_t>()->default_value(5000000), "Logical block size.")
    ;

    po::options_description cmd_opt;
    cmd_opt.add(desc);

    po::variables_map vm;

    try
    {
        po::store(
            po::command_line_parser(argc, argv).options(cmd_opt).run(), vm);
        po::notify(vm);
    }
    catch (const std::exception& e)
    {
        // Unknown options.
        cerr << e.what() << endl;
        cout << desc;
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        cout << desc;
        return EXIT_FAILURE;
    }

    output_t output_type = to_output_type(vm["output-type"].as<std::string>());

    if (output_type == output_t::unknown)
    {
        cerr << "Unknown output type." << endl;
        return EXIT_FAILURE;
    }

    int repeats = 3;

    const std::size_t test_sizes[] = {
        1024 * 1024 * 10,
        1024 * 1024 * 300,
        1024 * 1024 * 900,
    };

    for (std::size_t n_bytes : test_sizes)
    {
        for (int i = 0; i < repeats; ++i)
        {
            run_memcpy(n_bytes, output_type);
            run_copy(n_bytes, output_type);
        }

        for (int i = 0; i < repeats; ++i)
        {
            run_copy(n_bytes, output_type);
            run_memcpy(n_bytes, output_type);
        }
    }

    return EXIT_SUCCESS;
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */

