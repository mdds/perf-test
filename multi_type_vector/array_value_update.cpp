#include <iostream>
#include <vector>
#include <string>
#include <chrono>

#include <boost/program_options.hpp>

namespace po = boost::program_options;
using namespace std;

namespace {

class stack_printer
{
public:
    explicit stack_printer(const char* msg, bool print) :
        m_msg(msg), m_print(print)
    {
        if (m_print)
            std::cout << m_msg << ": --begin" << std::endl;
        m_start_time = get_time();
    }

    ~stack_printer()
    {
        double end_time = get_time();
        if (m_print)
            std::cout << m_msg << ": --end (duration: " << (end_time-m_start_time) << " sec)" << std::endl;
    }

    double get_duration() const
    {
        double end_time = get_time();
        return end_time - m_start_time;
    }

private:
    double get_time() const
    {
        int64_t usec_since_epoch =
            std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

        return usec_since_epoch / 1000000.0;
    }

    std::string m_msg;
    bool m_print;
    double m_start_time;
};

}

struct block
{
    size_t position;
    size_t size;
    void* data;

    block(size_t _position, size_t _size) :
        position(_position), size(_size), data(nullptr) {}
};

using blocks_type = std::vector<block>;

void warmup_loop(blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("warm-up loop", true);
    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        for (size_t i = 0; i < blocks.size(); ++i)
            blocks[i].position += delta;
    }
}

void loop_with_index_access(blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop with index access", true);
    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        for (size_t i = 0; i < blocks.size(); ++i)
            blocks[i].position += delta;
    }
}

void range_loop(blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("range loop", true);
    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        for (auto& b : blocks)
            b.position += delta;
    }
}

void loop_unrolling_4(
    blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop unrolling (4)", true);
    size_t rem = blocks.size() % 4;
    cout << "element size in last loop: " << rem << endl;

    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        size_t n = blocks.size() - rem;
        for (size_t i = 0; i < n; i += 4)
        {
            blocks[i].position += delta;
            blocks[i+1].position += delta;
            blocks[i+2].position += delta;
            blocks[i+3].position += delta;
        }

        for (size_t i = 0; i < rem; ++i)
            blocks[n+i].position += delta;
    }
}

void loop_unrolling_8(
    blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop unrolling (8)", true);
    size_t rem = blocks.size() % 8;
    cout << "element size in last loop: " << rem << endl;

    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        size_t n = blocks.size() - rem;
        for (size_t i = 0; i < n; i += 8)
        {
            blocks[i].position += delta;
            blocks[i+1].position += delta;
            blocks[i+2].position += delta;
            blocks[i+3].position += delta;
            blocks[i+4].position += delta;
            blocks[i+5].position += delta;
            blocks[i+6].position += delta;
            blocks[i+7].position += delta;
        }

        for (size_t i = 0; i < rem; ++i)
            blocks[n+i].position += delta;
    }
}

void loop_with_index_access_omp_parallel_for(blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop with index access (openmp parallel for)", true);
    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        #pragma omp parallel for
        for (long i = 0; i < blocks.size(); ++i)
            blocks[i].position += delta;
    }
}

#if 0
void loop_with_index_access_omp_simd(blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop with index access (openmp simd)", true);
    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        #pragma omp simd
        for (long i = 0; i < blocks.size(); ++i)
            blocks[i].position += delta;
    }
}

#endif

void loop_unrolling_4_omp_parallel_for(
    blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop unrolling (4) (openmp parallel for)", true);
    size_t rem = blocks.size() % 4;
    cout << "element size in last loop: " << rem << endl;

    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        long n = blocks.size() - rem;
        #pragma omp parallel for
        for (long i = 0; i < n; i += 4)
        {
            blocks[i].position += delta;
            blocks[i+1].position += delta;
            blocks[i+2].position += delta;
            blocks[i+3].position += delta;
        }

        for (size_t i = 0; i < rem; ++i)
            blocks[n+i].position += delta;
    }
}

#if 0

void loop_unrolling_4_omp_simd(
    blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop unrolling (4) (openmp simd)", true);
    size_t rem = blocks.size() % 4;
    cout << "element size in last loop: " << rem << endl;

    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        long n = blocks.size() - rem;
        #pragma omp simd
        for (long i = 0; i < n; i += 4)
        {
            blocks[i].position += delta;
            blocks[i+1].position += delta;
            blocks[i+2].position += delta;
            blocks[i+3].position += delta;
        }

        for (size_t i = 0; i < rem; ++i)
            blocks[n+i].position += delta;
    }
}

#endif

void loop_unrolling_8_omp_parallel_for(
    blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop unrolling (8) (openmp parallel for)", true);
    size_t rem = blocks.size() % 8;
    cout << "element size in last loop: " << rem << endl;

    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        long n = blocks.size() - rem;
        #pragma omp parallel for
        for (long i = 0; i < n; i += 8)
        {
            blocks[i].position += delta;
            blocks[i+1].position += delta;
            blocks[i+2].position += delta;
            blocks[i+3].position += delta;
            blocks[i+4].position += delta;
            blocks[i+5].position += delta;
            blocks[i+6].position += delta;
            blocks[i+7].position += delta;
        }

        for (size_t i = 0; i < rem; ++i)
            blocks[n+i].position += delta;
    }
}

#if 0

void loop_unrolling_8_omp_simd(
    blocks_type& blocks, size_t delta, size_t repeat_count)
{
    stack_printer __stack_printer__("loop unrolling (8) (openmp simd)", true);
    size_t rem = blocks.size() % 8;
    cout << "element size in last loop: " << rem << endl;

    for (size_t rep = 0; rep < repeat_count; ++rep)
    {
        long n = blocks.size() - rem;
        #pragma omp simd
        for (long i = 0; i < n; i += 8)
        {
            blocks[i].position += delta;
            blocks[i+1].position += delta;
            blocks[i+2].position += delta;
            blocks[i+3].position += delta;
            blocks[i+4].position += delta;
            blocks[i+5].position += delta;
            blocks[i+6].position += delta;
            blocks[i+7].position += delta;
        }

        for (size_t i = 0; i < rem; ++i)
            blocks[n+i].position += delta;
    }
}

#endif

int main(int argc, char** argv)
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Print this help.")
        ("sample-size,s", po::value<size_t>(), "Sample size.")
    ;

    po::options_description cmd_opt;
    cmd_opt.add(desc);

    po::variables_map vm;
    try
    {
        po::store(
            po::command_line_parser(argc, argv).options(cmd_opt).run(), vm);
        po::notify(vm);
    }
    catch (const exception& e)
    {
        // Unknown options.
        cerr << e.what() << endl;
        cout << desc;
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        cout << desc;
        return EXIT_FAILURE;
    }

    size_t sample_size = 50000;

    if (vm.count("sample-size"))
    {
        sample_size = vm["sample-size"].as<size_t>();
    }

    blocks_type blocks;
    for (size_t i = 0; i < sample_size; ++i)
        blocks.emplace_back(i, 1);

    cout << "block count: " << blocks.size() << endl;

    size_t delta = 2;
    warmup_loop(blocks, delta, sample_size / 10); // throw away this run.

    loop_unrolling_4(blocks, delta, sample_size);
    loop_unrolling_8(blocks, delta, sample_size);
    loop_with_index_access(blocks, delta, sample_size);
    range_loop(blocks, delta, sample_size);

    loop_unrolling_4_omp_parallel_for(blocks, delta, sample_size);
    loop_unrolling_8_omp_parallel_for(blocks, delta, sample_size);
    loop_with_index_access_omp_parallel_for(blocks, delta, sample_size);

#if 0
    loop_unrolling_4_omp_simd(blocks, delta, sample_size);
    loop_unrolling_8_omp_simd(blocks, delta, sample_size);
    loop_with_index_access_omp_simd(blocks, delta, sample_size);
#endif

    return EXIT_SUCCESS;
}
