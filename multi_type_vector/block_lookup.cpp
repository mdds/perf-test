
#include <mdds/multi_type_vector.hpp>
#include <mdds/multi_type_vector_trait.hpp>

#include <boost/program_options.hpp>

#include <iostream>
#include <string>
#include <chrono>

using namespace std;
namespace po = boost::program_options;

namespace {

class stack_printer
{
public:
    explicit stack_printer(const char* msg, bool print) :
        m_msg(msg), m_print(print)
    {
        if (m_print)
            std::cout << m_msg << ": --begin" << std::endl;
        m_start_time = get_time();
    }

    ~stack_printer()
    {
        double end_time = get_time();
        if (m_print)
            std::cout << m_msg << ": --end (duration: " << (end_time-m_start_time) << " sec)" << std::endl;
    }

    double get_duration() const
    {
        double end_time = get_time();
        return end_time - m_start_time;
    }

private:
    double get_time() const
    {
        int64_t usec_since_epoch =
            std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

        return usec_since_epoch / 1000000.0;
    }

    std::string m_msg;
    bool m_print;
    double m_start_time;
};

}

void run_no_position_hint(size_t size)
{
    using mtv_type = mdds::multi_type_vector<mdds::mtv::element_block_func>;

    // Initialize the container with one empty block of size 50000.
    mtv_type db(size);

    {
        stack_printer __stack_printer__("", false);

        // Set non-empty value at every other logical position from top down.
        for (size_t i = 0; i < size; ++i)
        {
            if (i % 2)
                db.set<double>(i, 1.0);
        }

        cout << "set,no hint," << size << ','  << __stack_printer__.get_duration() << endl;
    }

    {
        size_t last_pos = db.size() - 1;

        stack_printer __stack_printer__("", false);
        int32_t v = 0;
        for (size_t i = 0; i < size; ++i, ++v)
            db.set<int32_t>(last_pos, v);

        cout << "set last position,no hint," << size << ','  << __stack_printer__.get_duration() << endl;
    }

    {
        std::vector<float> vs(2, 99.0f);
        size_t insert_pos = size / 2;

        stack_printer __stack_printer__("", false);
        for (size_t i = 0; i < size; ++i)
            db.insert(insert_pos, vs.begin(), vs.end());

        cout << "insert mid-point,no hint," << size << ',' << __stack_printer__.get_duration() << endl;
    }
}

void run_with_position_hint(size_t size)
{
    using mtv_type = mdds::multi_type_vector<mdds::mtv::element_block_func>;

    // Initialize the container with one empty block of size 50000.
    mtv_type db(size);

    {
        mtv_type::iterator pos = db.begin();

        stack_printer __stack_printer__("", false);

        // Set non-empty value at every other logical position from top down.
        for (size_t i = 0; i < size; ++i)
        {
            if (i % 2)
                // Pass the position hint as the first argument, and receive a new
                // one returned from the method for the next call.
                pos = db.set<double>(pos, i, 1.0);
        }

        cout << "set,hint," << size << ','  << __stack_printer__.get_duration() << endl;
    }

    {
        size_t last_pos = db.size() - 1;
        mtv_type::iterator pos = db.position(last_pos).first;

        stack_printer __stack_printer__("", false);
        int32_t v = 0;
        for (size_t i = 0; i < size; ++i, ++v)
            db.set<int32_t>(pos, last_pos, v);

        cout << "set last position,hint," << size << ','  << __stack_printer__.get_duration() << endl;
    }

    {
        std::vector<float> vs(2, 99.0f);
        size_t insert_pos = size / 2;
        mtv_type::iterator pos = db.position(insert_pos).first;

        stack_printer __stack_printer__("", false);
        for (size_t i = 0; i < size; ++i)
            pos = db.insert(pos, insert_pos, vs.begin(), vs.end());

        cout << "insert mid-point,hint," << size << ',' << __stack_printer__.get_duration() << endl;
    }
}

int main(int argc, char** argv)
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Print this help.")
        ("size,s", po::value<size_t>()->default_value(50000), "Sample size.")
        ("header", po::value<uint16_t>()->default_value(0u)->implicit_value(1u), "Whether or not to print a header row.")
    ;

    po::options_description cmd_opt;
    cmd_opt.add(desc);

    po::variables_map vm;
    try
    {
        po::store(
            po::command_line_parser(argc, argv).options(cmd_opt).run(), vm);
        po::notify(vm);
    }
    catch (const exception& e)
    {
        // Unknown options.
        cerr << e.what() << endl;
        cout << desc;
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        cout << desc;
        return EXIT_FAILURE;
    }

    if (vm["header"].as<uint16_t>())
        cout << "type,position hint,size,duration" << endl;

    size_t size = vm["size"].as<size_t>();
    run_no_position_hint(size);
    run_with_position_hint(size);

    return EXIT_SUCCESS;
}
