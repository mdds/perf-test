
#include <mdds/multi_type_vector.hpp>
#include <mdds/multi_type_vector_trait.hpp>

#include <boost/program_options.hpp>

#include <iostream>
#include <string>
#include <chrono>

#ifdef _OPENMP
#define HAS_OPENMP 1
#else
#define HAS_OPENMP 0
#endif

#ifdef MDDS_LOOP_UNROLLING
#define LOOP_UNROLLING_ENABLED 1
#else
#define LOOP_UNROLLING_ENABLED 0
#endif

using namespace std;
namespace po = boost::program_options;

namespace {

class stack_printer
{
public:
    explicit stack_printer(const char* msg, bool print) :
        m_msg(msg), m_print(print)
    {
        if (m_print)
            std::cout << m_msg << ": --begin" << std::endl;
        m_start_time = get_time();
    }

    ~stack_printer()
    {
        double end_time = get_time();
        if (m_print)
            std::cout << m_msg << ": --end (duration: " << (end_time-m_start_time) << " sec)" << std::endl;
    }

    double get_duration() const
    {
        double end_time = get_time();
        return end_time - m_start_time;
    }

private:
    double get_time() const
    {
        int64_t usec_since_epoch =
            std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

        return usec_since_epoch / 1000000.0;
    }

    std::string m_msg;
    bool m_print;
    double m_start_time;
};

}

int main(int argc, char** argv)
{
    using mtv_type = mdds::multi_type_vector<mdds::mtv::element_block_func>;

    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Print this help.")
        ("debug,d", po::value<uint16_t>()->default_value(0u)->implicit_value(1u), "Whether to print debug statements.")
        ("size,s", po::value<size_t>()->default_value(50000), "Sample size.")
        ("header", po::value<uint16_t>()->default_value(0u)->implicit_value(1u), "Whether or not to print a header row.")
    ;

    po::options_description cmd_opt;
    cmd_opt.add(desc);

    po::variables_map vm;
    try
    {
        po::store(
            po::command_line_parser(argc, argv).options(cmd_opt).run(), vm);
        po::notify(vm);
    }
    catch (const exception& e)
    {
        // Unknown options.
        cerr << e.what() << endl;
        cout << desc;
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        cout << desc;
        return EXIT_FAILURE;
    }

    size_t size = vm["size"].as<size_t>();
    bool debug = vm["debug"].as<uint16_t>();
    bool header = vm["header"].as<uint16_t>();

    if (debug)
    {
        cout << "sample size: " << size << endl;
        cout << "openmp: " << HAS_OPENMP << endl;
        cout << "loop unrolling: " << LOOP_UNROLLING_ENABLED << endl;
    }

    if (header)
        cout << "openmp,loop unrolling,size,duration" << endl;

    mtv_type db(size);

    {
        mtv_type::iterator pos = db.begin();

        // Set non-empty value at every other logical position from top down.
        for (size_t i = 0; i < size; ++i)
        {
            if (i % 2)
                // Pass the position hint as the first argument, and receive a new
                // one returned from the method for the next call.
                pos = db.set<double>(pos, i, 1.0);
        }
    }

    {
        std::vector<float> vs(2, 99.0f);
        size_t insert_pos = size / 2;
        mtv_type::iterator pos = db.position(insert_pos).first;

        stack_printer __stack_printer__("insert mid-point", false);
        for (size_t i = 0; i < size; ++i)
            pos = db.insert(pos, insert_pos, vs.begin(), vs.end());

        cout << HAS_OPENMP << ',' << LOOP_UNROLLING_ENABLED << ',' << size << ',' << __stack_printer__.get_duration() << endl;
    }

    return EXIT_SUCCESS;
}

