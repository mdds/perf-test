#!/usr/bin/env python3

import argparse
import os.path
import sys
import subprocess


def _ints(s):
    return [int(x) for x in s.split(',')]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--bindir", type=str, required=True, help="Directory where the test executables are.")
    parser.add_argument("--sizes", "-s", type=_ints, required=True, help="Comma-separated list of numbers representing the sample sizes.")
    parser.add_argument("--repeat", "-r", type=int, default=3, help="Iteration size.")
    args = parser.parse_args()

    def ensure_executable(name):
        executable = os.path.join(args.bindir, name)
        try:
            subprocess.run([executable, "-h"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except:
            raise RuntimeError(f"executable named {executable} was expected but not found!")

        return executable

    exe_block_lookup = ensure_executable("block-lookup")

    exe_block_insertion = (
        ensure_executable("block-insertion"),
        ensure_executable("block-insertion-openmp"),
        ensure_executable("block-insertion-unroll"),
        ensure_executable("block-insertion-unroll-openmp"),
    )

    with open("block-insertions.csv", "w") as f:
        first = True
        for s in args.sizes:
            for i in range(args.repeat):
                print(f"block-insertion: size: {s}; run: {i}", flush=True)
                for exe in exe_block_insertion:
                    cmd = [exe, "-s", f"{s}"]
                    if first:
                        cmd.append("--header")
                        first = False
                    subprocess.run(cmd, stdout=f)

    with open("block-lookups.csv", "w") as f:
        first = True
        for s in args.sizes:
            for i in range(args.repeat):
                print(f"block-lookup: size: {s}; run: {i}", flush=True)
                cmd = [exe_block_lookup, "-s", f"{s}"]
                if first:
                    cmd.append("--header")
                    first = False
                subprocess.run(cmd, stdout=f)


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e, file=sys.stderr)
