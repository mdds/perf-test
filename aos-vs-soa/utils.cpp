
#include "common.hpp"

#include <sstream>
#include <iostream>
#include <chrono>
#include <algorithm>

using std::cout;
using std::cerr;
using std::endl;

std::size_t gen_random_size()
{
    // Marsaglia's xorshf generator
    static unsigned long x = 123456789, y = 362436069, z = 521288629;

    unsigned long t;

    x ^= x << 16;
    x ^= x >> 5;
    x ^= x << 1;

    t = x;
    x = y;
    y = z;
    z = t ^ x ^ y;

    std::size_t ret = (z & 0x7) + 1; // limit it to 1 - 8.
    return ret;
}

aos_blocks_t::iterator find_block(aos_blocks_t& blocks, std::size_t row)
{
    auto compare = [](const aos_block& l, const aos_block& r) -> bool
    {
        return l.position < r.position;
    };

    aos_block b(row, 0, nullptr);
    auto it0 = blocks.begin();
    auto it = std::lower_bound(it0, blocks.end(), b, compare);

    if (it == blocks.end() || it->position != row)
    {
        // Binary search has overshot by one block.  Move back one.
        --it;
    }

    return it;
}

template<typename _BLKS, typename _RT>
_RT soa_find_block(_BLKS& blocks, std::size_t row)
{
    auto it0 = blocks.position_array.begin();
    auto it = std::lower_bound(it0, blocks.position_array.end(), row);

    if (it == blocks.position_array.end() || *it != row)
    {
        // Binary search has overshot by one block.  Move back one.
        --it;
    }

    return std::distance(it0, it);
}

std::size_t find_block(soa_blocks_t& blocks, std::size_t row)
{
    return soa_find_block<soa_blocks_t, std::size_t>(blocks, row);
}

int64_t find_block(soa_blocks_aligned_16_t& blocks, int64_t row)
{
    return soa_find_block<soa_blocks_aligned_16_t, int64_t>(blocks, row);
}

int64_t find_block(soa_blocks_aligned_32_t& blocks, int64_t row)
{
    return soa_find_block<soa_blocks_aligned_32_t, int64_t>(blocks, row);
}

template<typename _BLKS>
bool soa_validate(const _BLKS& blocks)
{
    if (blocks.position_array.size() != blocks.size_array.size())
        return false;

    if (blocks.position_array.size() != blocks.data_array.size())
        return false;

    std::size_t pos = 0, n_blocks = blocks.position_array.size();

    for (std::size_t i = 0; i < n_blocks; ++i)
    {
        if (blocks.position_array[i] != pos)
            return false;

        pos += blocks.size_array[i];
    }

    return true;
}

bool validate(const soa_blocks_t& blocks)
{
    return soa_validate<soa_blocks_t>(blocks);
}

bool validate(const soa_blocks_aligned_16_t& blocks)
{
    if (!soa_validate<soa_blocks_aligned_16_t>(blocks))
        return false;

    const auto* RESTRICT p = blocks.position_array.data();

    auto rem = reinterpret_cast<const uintptr_t>(p) % 16u;

    if (rem)
    {
        cerr << "position array memory is not aligned as specified." << endl;
        return false;
    }

    p = blocks.size_array.data();
    rem = reinterpret_cast<const uintptr_t>(p) % 16u;

    if (rem)
    {
        cerr << "size array memory is not aligned as specified." << endl;
        return false;
    }

    return true;
}

bool validate(const soa_blocks_aligned_32_t& blocks)
{
    if (!soa_validate<soa_blocks_aligned_32_t>(blocks))
        return false;

    const auto* RESTRICT p = blocks.position_array.data();

    auto rem = reinterpret_cast<const uintptr_t>(p) % 32u;

    if (rem)
    {
        cerr << "position array memory is not aligned as specified." << endl;
        return false;
    }

    p = blocks.size_array.data();
    rem = reinterpret_cast<const uintptr_t>(p) % 32u;

    if (rem)
    {
        cerr << "size array memory is not aligned as specified." << endl;
        return false;
    }

    return true;
}

bool validate(const aos_blocks_t& blocks)
{
    std::size_t pos = 0;

    for (const aos_block& blk : blocks)
    {
        if (blk.position != pos)
        {
            cerr << "position (expected=" << pos << "; actual=" << blk.position << ")" << endl;
            return false;
        }

        pos += blk.size;
    }

    return true;
}

