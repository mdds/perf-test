
#include "common.hpp"

#include <iostream>
#include <boost/program_options.hpp>

using std::cout;
using std::cerr;
using std::endl;

namespace po = boost::program_options;

int main(int argc, char** argv)
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Print this help.")
        ("output-type,t", po::value<std::string>()->default_value("section"), "Runtime output type.")
        ("size,n", po::value<std::size_t>()->default_value(5000000), "Logical block size.")
    ;

    po::options_description cmd_opt;
    cmd_opt.add(desc);

    po::variables_map vm;

    try
    {
        po::store(
            po::command_line_parser(argc, argv).options(cmd_opt).run(), vm);
        po::notify(vm);
    }
    catch (const std::exception& e)
    {
        // Unknown options.
        cerr << e.what() << endl;
        cout << desc;
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        cout << desc;
        return EXIT_FAILURE;
    }

    output_t output_type = to_output_type(vm["output-type"].as<std::string>());

    if (output_type == output_t::unknown)
    {
        cerr << "Unknown output type." << endl;
        return EXIT_FAILURE;
    }

    std::size_t block_size = vm["size"].as<std::size_t>();

    {
        aos_blocks_t aos_blocks = init_aos_blocks(block_size, output_type);
        if (!validate(aos_blocks))
        {
            cerr << "validation failed on the AoS blocks." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks_t soa_blocks = init_soa_blocks(block_size, output_type);

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks." << endl;
            return EXIT_FAILURE;
        }
    }

    {
        aos_blocks_t aos_blocks = init_aos_blocks_push_back(block_size, output_type);
        if (!validate(aos_blocks))
        {
            cerr << "validation failed on the AoS blocks." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks_t soa_blocks = init_soa_blocks_push_back(block_size, output_type);
        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks." << endl;
            return EXIT_FAILURE;
        }

        if (output_type == output_t::section)
        {
            cout << "--" << endl;
            cout << "aos-block size: " << sizeof(aos_block) << endl;
        }

        lookup_aos_blocks(aos_blocks, output_type);
        lookup_soa_blocks(soa_blocks, output_type);

        std::vector<std::size_t> size_diffs;
        for (int repeat = 0; repeat < 100; ++repeat)
            size_diffs.push_back(gen_random_size());

        auto aos_blocks_saved = aos_blocks;

        {
            stack_printer __stack_printer__("insert,aos,insert_aos_blocks_unroll8", output_type);

            for (auto diff : size_diffs)
                insert_aos_blocks_unroll8(aos_blocks, 0, diff);
        }

        if (!validate(aos_blocks))
        {
            cerr << "validation failed on the AoS blocks after insertion (unroll8)." << endl;
            return EXIT_FAILURE;
        }

        aos_blocks = aos_blocks_saved;

        {
            stack_printer __stack_printer__("insert,aos,insert_aos_blocks_unroll4", output_type);

            for (auto diff : size_diffs)
                insert_aos_blocks_unroll4(aos_blocks, 0, diff);
        }

        if (!validate(aos_blocks))
        {
            cerr << "validation failed on the AoS blocks after insertion (unroll4)." << endl;
            return EXIT_FAILURE;
        }

        aos_blocks = aos_blocks_saved;

        {
            stack_printer __stack_printer__("insert,aos,insert_aos_blocks", output_type);

            for (auto diff : size_diffs)
                insert_aos_blocks(aos_blocks, 0, diff);
        }

        if (!validate(aos_blocks))
        {
            cerr << "validation failed on the AoS blocks after insertion." << endl;
            return EXIT_FAILURE;
        }

        auto soa_blocks_saved = soa_blocks;

        soa_blocks = soa_blocks_saved;

        {
            stack_printer __stack_printer__("insert,soa,insert_soa_blocks_unroll16", output_type);
            for (auto diff : size_diffs)
                insert_soa_blocks_unroll16(soa_blocks, 0, diff);
        }

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks after insertion (unroll16)." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks = soa_blocks_saved;

        {
            stack_printer __stack_printer__("insert,soa,insert_soa_blocks_unroll16_omp", output_type);
            for (auto diff : size_diffs)
                insert_soa_blocks_unroll16_omp(soa_blocks, 0, diff);
        }

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks after insertion (unroll16 omp)." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks = soa_blocks_saved;

        {
            stack_printer __stack_printer__("insert,soa,insert_soa_blocks_unroll8", output_type);
            for (auto diff : size_diffs)
                insert_soa_blocks_unroll8(soa_blocks, 0, diff);
        }

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks after insertion (unroll8)." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks = soa_blocks_saved;

        {
            stack_printer __stack_printer__("insert,soa,insert_soa_blocks_unroll8_omp", output_type);
            for (auto diff : size_diffs)
                insert_soa_blocks_unroll8_omp(soa_blocks, 0, diff);
        }

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks after insertion (unroll8 omp)." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks = soa_blocks_saved;

        {
            stack_printer __stack_printer__("insert,soa,insert_soa_blocks_unroll4", output_type);
            for (auto diff : size_diffs)
                insert_soa_blocks_unroll4(soa_blocks, 0, diff);
        }

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks after insertion (unroll4)." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks = soa_blocks_saved;

        {
            stack_printer __stack_printer__("insert,soa,insert_soa_blocks_unroll4_omp", output_type);
            for (auto diff : size_diffs)
                insert_soa_blocks_unroll4_omp(soa_blocks, 0, diff);
        }

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks after insertion (unroll4 omp)." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks = soa_blocks_saved;

        {
            stack_printer __stack_printer__("insert,soa,insert_soa_blocks", output_type);
            for (auto diff : size_diffs)
                insert_soa_blocks(soa_blocks, 0, diff);
        }

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks after insertion." << endl;
            return EXIT_FAILURE;
        }

        soa_blocks = soa_blocks_saved;

        {
            stack_printer __stack_printer__("insert,soa,insert_soa_blocks_omp", output_type);
            for (auto diff : size_diffs)
                insert_soa_blocks_omp(soa_blocks, 0, diff);
        }

        if (!validate(soa_blocks))
        {
            cerr << "validation failed on the SoA blocks after insertion (omp)." << endl;
            return EXIT_FAILURE;
        }

        if (sizeof(std::size_t) == 8)
        {
            soa_blocks = soa_blocks_saved;

            {
                stack_printer __stack_printer__("insert,soa,insert_soa_blocks_sse2", output_type);
                for (auto diff : size_diffs)
                    insert_soa_blocks_sse2(soa_blocks, 0, diff);
            }

            if (!validate(soa_blocks))
            {
                cerr << "validation failed on the SoA blocks after insertion (sse2)." << endl;
                return EXIT_FAILURE;
            }

            soa_blocks = soa_blocks_saved;

            {
                stack_printer __stack_printer__("insert,soa,insert_soa_blocks_sse2_omp", output_type);
                for (auto diff : size_diffs)
                    insert_soa_blocks_sse2_omp(soa_blocks, 0, diff);
            }

            if (!validate(soa_blocks))
            {
                cerr << "validation failed on the SoA blocks after insertion (sse2 omp)." << endl;
                return EXIT_FAILURE;
            }
        }

        {
            soa_blocks_aligned_16_t soa_blocks_a16 = init_soa_blocks_aligned_16(block_size, output_type);

            if (!validate(soa_blocks_a16))
            {
                cerr << "validation failed on the aligned SoA blocks." << endl;
                return EXIT_FAILURE;
            }

            {
                stack_printer __stack_printer__("insert,soa,insert_soa_blocks_sse2 (aligned 16)", output_type);
                for (auto diff : size_diffs)
                    insert_soa_blocks_sse2_aligned_16(soa_blocks_a16, 0, diff);
            }

            if (!validate(soa_blocks_a16))
            {
                cerr << "validation failed on the SoA blocks after insertion (sse2; aligned 16)." << endl;
                return EXIT_FAILURE;
            }
        }

#ifdef __AVX2__
        {
            soa_blocks = soa_blocks_saved;

            {
                stack_printer __stack_printer__("insert,soa,insert_soa_blocks_avx2", output_type);
                for (auto diff : size_diffs)
                    insert_soa_blocks_avx2(soa_blocks, 0, diff);
            }

            if (!validate(soa_blocks))
            {
                cerr << "validation failed on the SoA blocks after insertion (avx)." << endl;
                return EXIT_FAILURE;
            }
        }

        {
            soa_blocks = soa_blocks_saved;

            {
                stack_printer __stack_printer__("insert,soa,insert_soa_blocks_avx2_omp", output_type);
                for (auto diff : size_diffs)
                    insert_soa_blocks_avx2_omp(soa_blocks, 0, diff);
            }

            if (!validate(soa_blocks))
            {
                cerr << "validation failed on the SoA blocks after insertion (avx2 omp)." << endl;
                return EXIT_FAILURE;
            }
        }

        {
            soa_blocks_aligned_32_t soa_blocks_a32 = init_soa_blocks_aligned_32(block_size, output_type);
            if (!validate(soa_blocks_a32))
            {
                cerr << "validation failed on the aligned SoA blocks." << endl;
                return EXIT_FAILURE;
            }

            {
                stack_printer __stack_printer__("insert,soa,insert_soa_blocks_avx2 (aligned 32)", output_type);
                for (auto diff : size_diffs)
                    insert_soa_blocks_avx2_aligned_32(soa_blocks_a32, 0, diff);
            }

            if (!validate(soa_blocks_a32))
            {
                cerr << "validation failed on the SoA blocks after insertion (avx2; aligned 32)." << endl;
                return EXIT_FAILURE;
            }
        }
#endif
    }

    return EXIT_SUCCESS;
}
