#!/usr/bin/env bash

EXEC="$1"

if [ -z "$EXEC" ]; then
    >&2 echo "Test program must be specified."
    exit 1
fi

BLOCK_SIZES=(100000 1000000 5000000 10000000)

echo "Category,Block Type,Test,Duration,Block Size,Run ID"

RUN_ID=1
while [ $RUN_ID -lt 6 ]; do
    for BLOCK_SIZE in ${BLOCK_SIZES[@]}; do
        "$EXEC" -n $BLOCK_SIZE -t report | sed -e "s/\$/,$BLOCK_SIZE,$RUN_ID/g"
    done

    let RUN_ID=RUN_ID+1
    sleep 5
done


