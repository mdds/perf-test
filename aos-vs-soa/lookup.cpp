
#include "common.hpp"

#include <sstream>
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

namespace {

void* p_deadbeef = reinterpret_cast<void*>(0xDEADBEEF);

}

void lookup_aos_blocks(aos_blocks_t& blocks, output_t ot)
{
    std::size_t end_row_position = blocks.back().position + blocks.back().size;
    {
        stack_printer __stack_printer__("lookup,aos,lookup_aos_blocks", ot);
        if (ot == output_t::section)
        {
            cout << "end row position: " << end_row_position << endl;
            cout << "block size: " << blocks.size() << endl;
        }

        for (std::size_t row = 0; row < end_row_position; ++row)
        {
            auto it = find_block(blocks, row);
            it->data = p_deadbeef;
        }
    }

    // Make sure the data are all set.
    for (const auto& block : blocks)
    {
        if (block.data != p_deadbeef)
        {
            std::ostringstream os;
            os << "data pointer is not DEADBEEF (position=" << block.position << "; size=" << block.size << ")";
            throw std::runtime_error(os.str());
        }
    }
}

void lookup_soa_blocks(soa_blocks_t& blocks, output_t ot)
{
    std::size_t end_row_position = blocks.position_array.back() + blocks.size_array.back();

    {
        stack_printer __stack_printer__("lookup,soa,lookup_soa_blocks", ot);
        if (ot == output_t::section)
        {
            cout << "end row position: " << end_row_position << endl;
            cout << "block size: " << blocks.position_array.size() << endl;
        }

        for (std::size_t row = 0; row < end_row_position; ++row)
        {
            std::size_t pos = find_block(blocks, row);
            blocks.data_array[pos] = p_deadbeef;
        }
    }

    // Make sure the data are all set.
    for (const auto& v : blocks.data_array)
    {
        if (v != p_deadbeef)
        {
            std::size_t pos = &v - blocks.data_array.data();
            std::size_t position = blocks.position_array[pos];
            std::size_t size = blocks.size_array[pos];
            std::ostringstream os;
            os << "data pointer is not DEADBEEF (position=" << position << "; size=" << size << ")";
            throw std::runtime_error(os.str());
        }
    }
}

