#pragma once

#include "utils.hpp"

#include <string>
#include <vector>

#include <boost/align/aligned_allocator.hpp>

#ifndef L1_CACHE_LINESIZE
#define L1_CACHE_LINESIZE 64
#endif

#if defined _WIN32
#define RESTRICT __restrict
#define ALIGNED(a) __declspec(align(a))
#else
#define RESTRICT __restrict__
#define ALIGNED(a) __attribute__((aligned(a)))
#endif

std::size_t gen_random_size();

struct aos_block
{
    std::size_t position;
    std::size_t size;
    void* data;

    aos_block(std::size_t _position, std::size_t _size, void* _data) :
        position(_position), size(_size), data(_data) {}

    aos_block() : position(0), size(0), data(nullptr) {}
};

using aos_blocks_t = std::vector<aos_block>;

struct soa_blocks_t
{
    std::vector<std::size_t> position_array;
    std::vector<std::size_t> size_array;
    std::vector<void*> data_array;

    soa_blocks_t() {}

    soa_blocks_t(std::size_t size) :
        position_array(size, 0),
        size_array(size, 0),
        data_array(size, nullptr)
    {
    }

    void reserve(std::size_t capacity)
    {
        position_array.reserve(capacity);
        size_array.reserve(capacity);
        data_array.reserve(capacity);
    }
};

/** consists of 16-byte aligned arrays. */
struct soa_blocks_aligned_16_t
{
    using int64_array_type =
        std::vector<int64_t, boost::alignment::aligned_allocator<int64_t, 16>>;

    int64_array_type position_array;
    int64_array_type size_array;
    std::vector<void*> data_array;

    soa_blocks_aligned_16_t() {}

    soa_blocks_aligned_16_t(int64_t size) :
        position_array(size, 0),
        size_array(size, 0),
        data_array(size, nullptr)
    {
    }

    void reserve(std::size_t capacity)
    {
        position_array.reserve(capacity);
        size_array.reserve(capacity);
        data_array.reserve(capacity);
    }
};

struct soa_blocks_aligned_32_t
{
    using int64_array_type =
        std::vector<int64_t, boost::alignment::aligned_allocator<int64_t, 32>>;

    int64_array_type position_array;
    int64_array_type size_array;
    std::vector<void*> data_array;

    soa_blocks_aligned_32_t() {}

    soa_blocks_aligned_32_t(int64_t size) :
        position_array(size, 0),
        size_array(size, 0),
        data_array(size, nullptr)
    {
    }

    void reserve(std::size_t capacity)
    {
        position_array.reserve(capacity);
        size_array.reserve(capacity);
        data_array.reserve(capacity);
    }
};

using int64_a16_t = ALIGNED(16) int64_t;
using int64_a32_t = ALIGNED(32) int64_t;

aos_blocks_t::iterator find_block(aos_blocks_t& blocks, std::size_t row);

std::size_t find_block(soa_blocks_t& blocks, std::size_t row);

int64_t find_block(soa_blocks_aligned_16_t& blocks, int64_t row);

int64_t find_block(soa_blocks_aligned_32_t& blocks, int64_t row);

bool validate(const soa_blocks_t& blocks);

bool validate(const soa_blocks_aligned_16_t& blocks);

bool validate(const soa_blocks_aligned_32_t& blocks);

bool validate(const aos_blocks_t& blocks);

aos_blocks_t init_aos_blocks(std::size_t n_blocks, output_t ot);

aos_blocks_t init_aos_blocks_push_back(std::size_t n_blocks, output_t ot);

soa_blocks_t init_soa_blocks(std::size_t n_blocks, output_t ot);

soa_blocks_aligned_16_t init_soa_blocks_aligned_16(std::size_t n_blocks, output_t ot);

soa_blocks_aligned_32_t init_soa_blocks_aligned_32(std::size_t n_blocks, output_t ot);

soa_blocks_t init_soa_blocks_push_back(std::size_t n_blocks, output_t ot);

void lookup_aos_blocks(aos_blocks_t& blocks, output_t ot);

void lookup_soa_blocks(soa_blocks_t& blocks, output_t ot);

void insert_aos_blocks(aos_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_aos_blocks_unroll4(aos_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_aos_blocks_unroll8(aos_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_unroll4(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_unroll4_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_unroll8(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_unroll8_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_unroll16(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_unroll16_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_sse2(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_sse2_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_sse2_aligned_16(soa_blocks_aligned_16_t& blocks, int64_t row, int64_a16_t size);

#ifdef __AVX2__

void insert_soa_blocks_avx2(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_avx2_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size);

void insert_soa_blocks_avx2_aligned_32(soa_blocks_aligned_32_t& blocks, std::size_t row, int64_a32_t size);

#endif
