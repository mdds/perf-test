
#include "common.hpp"

aos_blocks_t init_aos_blocks(std::size_t n_blocks, output_t ot)
{
    stack_printer __stack_printer__("init,aos,init_aos_blocks", ot);

    aos_blocks_t blocks(n_blocks);

    std::size_t position = 0;

    for (auto& blk : blocks)
    {
        blk.position = position;
        blk.size = gen_random_size();
        blk.data = nullptr;
        position += blk.size;
    }

    return blocks;
}

aos_blocks_t init_aos_blocks_push_back(std::size_t n_blocks, output_t ot)
{
    stack_printer __stack_printer__("init,aos,init_aos_blocks_push_back", ot);

    aos_blocks_t blocks;
    blocks.reserve(n_blocks);

    std::size_t position = 0;

    for (std::size_t pos = 0; pos < n_blocks; ++pos)
    {
        blocks.emplace_back(position, gen_random_size(), nullptr);
        position += blocks.back().size;
    }

    return blocks;
}

soa_blocks_t init_soa_blocks(std::size_t n_blocks, output_t ot)
{
    stack_printer __stack_printer__("init,soa,init_soa_blocks", ot);

    soa_blocks_t blocks(n_blocks);

    std::size_t position = 0;

    for (std::size_t pos = 0; pos < n_blocks; ++pos)
    {
        blocks.position_array[pos] = position;
        blocks.size_array[pos] = gen_random_size();
        blocks.data_array[pos] = nullptr;
        position += blocks.size_array[pos];
    }

    return blocks;
}

soa_blocks_aligned_16_t init_soa_blocks_aligned_16(std::size_t n_blocks, output_t ot)
{
    stack_printer __stack_printer__("init,soa,init_soa_blocks_aligned_16", ot);

    soa_blocks_aligned_16_t blocks(n_blocks);

    int64_t position = 0;

    for (std::size_t pos = 0; pos < n_blocks; ++pos)
    {
        blocks.position_array[pos] = position;
        blocks.size_array[pos] = gen_random_size();
        blocks.data_array[pos] = nullptr;
        position += blocks.size_array[pos];
    }

    return blocks;
}

soa_blocks_aligned_32_t init_soa_blocks_aligned_32(std::size_t n_blocks, output_t ot)
{
    stack_printer __stack_printer__("init,soa,init_soa_blocks_aligned_32", ot);

    soa_blocks_aligned_32_t blocks(n_blocks);

    int64_t position = 0;

    for (std::size_t pos = 0; pos < n_blocks; ++pos)
    {
        blocks.position_array[pos] = position;
        blocks.size_array[pos] = gen_random_size();
        blocks.data_array[pos] = nullptr;
        position += blocks.size_array[pos];
    }

    return blocks;
}

soa_blocks_t init_soa_blocks_push_back(std::size_t n_blocks, output_t ot)
{
    stack_printer __stack_printer__("init,soa,init_soa_blocks_push_back", ot);

    soa_blocks_t blocks;
    blocks.reserve(n_blocks);

    std::size_t position = 0;

    for (std::size_t pos = 0; pos < n_blocks; ++pos)
    {
        blocks.position_array.push_back(position);
        blocks.size_array.push_back(gen_random_size());
        blocks.data_array.push_back(nullptr);
        position += blocks.size_array.back();
    }

    return blocks;
}
