#include "common.hpp"

#include <iostream>
#include <emmintrin.h>
#include <immintrin.h>

using std::cout;
using std::endl;

void insert_aos_blocks(aos_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto it = find_block(blocks, row);
    it->size += size;

    std::size_t pos = std::distance(blocks.begin(), it);

    for (++pos; pos < blocks.size(); ++pos)
        blocks[pos].position += size;
}

void insert_aos_blocks_unroll4(aos_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto it = find_block(blocks, row);
    it->size += size;

    std::size_t offset = std::distance(blocks.begin(), it);

    ++offset;

    std::size_t n_blocks = blocks.size() - offset;
    std::size_t rem = n_blocks & 0x3; // % 4
    std::size_t n_loops = n_blocks >> 2;

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 2);
        blocks[pos].position += size;
        blocks[pos+1].position += size;
        blocks[pos+2].position += size;
        blocks[pos+3].position += size;
    }

    std::size_t pos = blocks.size() - rem;
    for (; pos < blocks.size(); ++pos)
        blocks[pos].position += size;
}

void insert_aos_blocks_unroll8(aos_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto it = find_block(blocks, row);
    it->size += size;

    std::size_t offset = std::distance(blocks.begin(), it);

    ++offset;

    std::size_t n_blocks = blocks.size() - offset;
    std::size_t rem = n_blocks & 0x7; // % 8
    std::size_t n_loops = n_blocks >> 3;

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 3);
        blocks[pos].position += size;
        blocks[pos+1].position += size;
        blocks[pos+2].position += size;
        blocks[pos+3].position += size;
        blocks[pos+4].position += size;
        blocks[pos+5].position += size;
        blocks[pos+6].position += size;
        blocks[pos+7].position += size;
    }

    std::size_t pos = blocks.size() - rem;
    for (; pos < blocks.size(); ++pos)
        blocks[pos].position += size;
}

void insert_soa_blocks(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto pos = find_block(blocks, row);
    blocks.size_array[pos] += size;

    for (++pos; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto pos = find_block(blocks, row);
    blocks.size_array[pos] += size;

    ++pos;
    int64_t block_size = blocks.position_array.size();
    #pragma omp parallel for
    for (int64_t i = pos; i < block_size; ++i)
        blocks.position_array[i] += size;
}

void insert_soa_blocks_unroll4(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0x3; // % 4
    std::size_t n_loops = n_blocks >> 2;

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 2);
        blocks.position_array[pos] += size;
        blocks.position_array[pos+1] += size;
        blocks.position_array[pos+2] += size;
        blocks.position_array[pos+3] += size;
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_unroll4_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0x3; // % 4
    int64_t n_loops = n_blocks >> 2;

    #pragma omp parallel for
    for (int64_t i = 0; i < n_loops; ++i)
    {
        int64_t pos = offset + (i << 2);
        blocks.position_array[pos] += size;
        blocks.position_array[pos+1] += size;
        blocks.position_array[pos+2] += size;
        blocks.position_array[pos+3] += size;
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_unroll8(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0x7; // % 8
    std::size_t n_loops = n_blocks >> 3;

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 3);
        blocks.position_array[pos] += size;
        blocks.position_array[pos+1] += size;
        blocks.position_array[pos+2] += size;
        blocks.position_array[pos+3] += size;
        blocks.position_array[pos+4] += size;
        blocks.position_array[pos+5] += size;
        blocks.position_array[pos+6] += size;
        blocks.position_array[pos+7] += size;
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_unroll8_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0x7; // % 8
    int64_t n_loops = n_blocks >> 3;

    #pragma omp parallel for
    for (int64_t i = 0; i < n_loops; ++i)
    {
        int64_t pos = offset + (i << 3);
        blocks.position_array[pos] += size;
        blocks.position_array[pos+1] += size;
        blocks.position_array[pos+2] += size;
        blocks.position_array[pos+3] += size;
        blocks.position_array[pos+4] += size;
        blocks.position_array[pos+5] += size;
        blocks.position_array[pos+6] += size;
        blocks.position_array[pos+7] += size;
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_unroll16(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0xF; // % 16
    std::size_t n_loops = n_blocks >> 4;

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 4);
        blocks.position_array[pos] += size;
        blocks.position_array[pos+1] += size;
        blocks.position_array[pos+2] += size;
        blocks.position_array[pos+3] += size;
        blocks.position_array[pos+4] += size;
        blocks.position_array[pos+5] += size;
        blocks.position_array[pos+6] += size;
        blocks.position_array[pos+7] += size;
        blocks.position_array[pos+8] += size;
        blocks.position_array[pos+9] += size;
        blocks.position_array[pos+10] += size;
        blocks.position_array[pos+11] += size;
        blocks.position_array[pos+12] += size;
        blocks.position_array[pos+13] += size;
        blocks.position_array[pos+14] += size;
        blocks.position_array[pos+15] += size;
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_unroll16_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0xF; // % 16
    int64_t n_loops = n_blocks >> 4;

    #pragma omp parallel for
    for (int64_t i = 0; i < n_loops; ++i)
    {
        int64_t pos = offset + (i << 4);
        blocks.position_array[pos] += size;
        blocks.position_array[pos+1] += size;
        blocks.position_array[pos+2] += size;
        blocks.position_array[pos+3] += size;
        blocks.position_array[pos+4] += size;
        blocks.position_array[pos+5] += size;
        blocks.position_array[pos+6] += size;
        blocks.position_array[pos+7] += size;
        blocks.position_array[pos+8] += size;
        blocks.position_array[pos+9] += size;
        blocks.position_array[pos+10] += size;
        blocks.position_array[pos+11] += size;
        blocks.position_array[pos+12] += size;
        blocks.position_array[pos+13] += size;
        blocks.position_array[pos+14] += size;
        blocks.position_array[pos+15] += size;
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_sse2(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    bool rem = n_blocks & 0x1; // % 2
    std::size_t n_loops = n_blocks >> 1;

    __m128i right = _mm_set_epi64x(size, size);

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 1);
        __m128i* dst = (__m128i*)&blocks.position_array[pos];
        __m128i left = _mm_loadu_si128(dst);
        left = _mm_add_epi64(left, right);
        _mm_storeu_si128(dst, left);
    }

    if (rem)
        blocks.position_array.back() += size;
}

void insert_soa_blocks_sse2_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    bool rem = n_blocks & 0x1; // % 2
    int64_t n_loops = n_blocks >> 1;

    __m128i right = _mm_set_epi64x(size, size);

    #pragma omp parallel for
    for (int64_t i = 0; i < n_loops; ++i)
    {
        int64_t pos = offset + (i << 1);
        __m128i* dst = (__m128i*)&blocks.position_array[pos];
        __m128i left = _mm_loadu_si128(dst);
        left = _mm_add_epi64(left, right);
        _mm_storeu_si128(dst, left);
    }

    if (rem)
        blocks.position_array.back() += size;
}

void insert_soa_blocks_sse2_aligned_16(soa_blocks_aligned_16_t& blocks, int64_t row, int64_a16_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    bool rem = n_blocks & 0x1; // % 2
    std::size_t n_loops = n_blocks >> 1;

    __m128i right = _mm_set_epi64x(size, size);

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 1);
        __m128i* RESTRICT dst = (__m128i*)&blocks.position_array[pos];
        __m128i left = _mm_loadu_si128(dst);
        left = _mm_add_epi64(left, right);
        _mm_storeu_si128(dst, left);
    }

    if (rem)
        blocks.position_array.back() += size;
}

#ifdef __AVX2__

void insert_soa_blocks_avx2(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0x3; // % 4
    std::size_t n_loops = n_blocks >> 2;

    __m256i right = _mm256_set1_epi64x(size);

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 2);
        __m256i* dst = reinterpret_cast<__m256i*>(&blocks.position_array[pos]);
        __m256i left = _mm256_loadu_si256(dst);
        left = _mm256_add_epi64(left, right);
        _mm256_storeu_si256(dst, left);
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_avx2_omp(soa_blocks_t& blocks, std::size_t row, std::size_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0x3; // % 4
    int64_t n_loops = n_blocks >> 2;

    __m256i right = _mm256_set1_epi64x(size);

    #pragma omp parallel for
    for (int64_t i = 0; i < n_loops; ++i)
    {
        int64_t pos = offset + (i << 2);
        __m256i* dst = reinterpret_cast<__m256i*>(&blocks.position_array[pos]);
        __m256i left = _mm256_loadu_si256(dst);
        left = _mm256_add_epi64(left, right);
        _mm256_storeu_si256(dst, left);
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

void insert_soa_blocks_avx2_aligned_32(soa_blocks_aligned_32_t& blocks, std::size_t row, int64_a32_t size)
{
    auto offset = find_block(blocks, row);
    blocks.size_array[offset] += size;

    ++offset;

    std::size_t n_blocks = blocks.position_array.size() - offset;
    std::size_t rem = n_blocks & 0x3; // % 4
    std::size_t n_loops = n_blocks >> 2;

    __m256i right = _mm256_set1_epi64x(size);

    for (std::size_t i = 0; i < n_loops; ++i)
    {
        std::size_t pos = offset + (i << 2);
        __m256i* RESTRICT dst = reinterpret_cast<__m256i*>(&blocks.position_array[pos]);
        __m256i left = _mm256_loadu_si256(dst);
        left = _mm256_add_epi64(left, right);
        _mm256_storeu_si256(dst, left);
    }

    std::size_t pos = blocks.position_array.size() - rem;
    for (; pos < blocks.position_array.size(); ++pos)
        blocks.position_array[pos] += size;
}

#endif
