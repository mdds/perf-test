/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * Copyright (c) 2021 Kohei Yoshida
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 ************************************************************************/

#include "utils.hpp"

#include <iostream>
#include <chrono>
#include <sstream>

output_t to_output_type(std::string_view s)
{
    std::vector<std::pair<const char*, output_t>> table =
    {
        { "section", output_t::section },
        { "report", output_t::report },
    };

    for (const auto& entry : table)
    {
        if (s == entry.first)
            return entry.second;
    }

    return output_t::unknown;
}

stack_printer::stack_printer(std::string msg, output_t ot) :
    m_msg(std::move(msg)), m_out_type(ot)
{
    if (m_out_type == output_t::section)
        std::cout << m_msg << ": --begin" << std::endl;

    m_start_time = get_time();
}

stack_printer::~stack_printer()
{
    double end_time = get_time();

    switch (m_out_type)
    {
        case output_t::section:
            std::cout << m_msg << ": --end (duration: " << (end_time-m_start_time) << " sec)" << std::endl;
            break;
        case output_t::report:
            std::cout << m_msg << ',' << (end_time-m_start_time) << std::endl;
            break;
    }
}

void stack_printer::print_time(int line) const
{
    if (m_out_type == output_t::section)
    {
        double end_time = get_time();
        std::cout << m_msg << ": --(" << line << ") (duration: " << (end_time-m_start_time) << " sec)" << std::endl;
    }
}

double stack_printer::get_time() const
{
    unsigned long usec_since_epoch =
        std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now().time_since_epoch()).count();

    return usec_since_epoch / 1000000.0;
}

std::string to_size(std::size_t size)
{
    const std::vector<const char*> units = {
        nullptr, "KiB", "MiB", "GiB"
    };

    float size_f = size;

    auto unit = units.begin();

    for (; unit != units.end(); ++unit)
    {
        float test = size_f / 1024.0f;
        if (test <= 1.0f)
            break;

        size_f = test;
    }

    const char* unit_s = *unit;

    std::ostringstream os;

    if (unit_s)
        os << size_f << ' ' << unit_s << " (" << size << " bytes)";
    else
        os << size << " bytes";

    return os.str();
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */

